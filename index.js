let todos;
const todoInput = document.querySelector("#todoInput");
let addTodoBtn = document.querySelector("#addTodo");

const setPrevTodos = () => {
  const isInLocal = localStorage.getItem("todos");
  if (!JSON.parse(isInLocal)) {
    todos = [];
  } else {
    todos = JSON.parse(isInLocal);
  }
  return todos;
};

// add todo
const addTodo = (e) => {
  const newTodo = {};
  addTodoBtn.addEventListener("click", (e) => {
    const todoVal = todoInput.value;
    if (todoVal !== "") {
      newTodo.todoInfo = todoVal;
      newTodo.isComplete = false;
      newTodo.id = Date.now();
      console.log("New Todo", newTodo);
      console.log("All Todos", todos);
      todoInput.value = "";
      setInLocalStorage(newTodo);
      showTodos();
    }
  });
};
addTodo();
// add todo
// set items in localstorage
const setInLocalStorage = (todo) => {
  const isInLocal = localStorage.getItem("todos");
  if (!JSON.parse(isInLocal)) {
    todos = [];
  } else {
    todos = JSON.parse(isInLocal);
  }
  todos.unshift(todo);
  localStorage.setItem("todos", JSON.stringify(todos));
};
// set items in localstorage
//show filtered todos
const showFilteredTodos = (arr, isPending) => {
  const pendingItems = document.querySelector("#pending");
  const completedItems = document.querySelector("#completed");

  if (isPending == true) {
    const pendingTodos = arr.map((todo) => {
      if (todo.isComplete === false) {
        return ` <li id="${todo?.id}" class="mb-2 last:mb-0">
      <div class="w-full flex items-center space-x-1">
          <div class="h-8 bg-gray-50 w-[80%] flex items-center px-4">
              <p class="">${todo?.todoInfo}</p>
          </div>
          <div onclick={updateTodo(${todo?.id})}
              class="w-[10%] h-8 flex justify-center items-center text-white cursor-pointer bg-green-600">
              <i class="fa-solid fa-check"></i>
          </div>
          <div onclick={deleteTodo(${todo?.id})}
              class="w-[10%] h-8 flex justify-center items-center text-white cursor-pointer bg-pink-700">
              <i class="fa-solid fa-trash-can"></i>
          </div>
      </div>
  </li>`;
      }
    });
    pendingItems.innerHTML = pendingTodos.join("");
  } else {
    const completedTodos = arr.map((todo) => {
      if (todo?.isComplete == true) {
        return ` <li id="${todo?.id}" class="mb-2 last:mb-0">
        <div class="w-full flex items-center space-x-1">
            <div class="h-8 bg-gray-50 w-[80%] flex items-center px-4">
                <p class="line-through w-full">${todo?.todoInfo}</p>
            </div>
            <div onclick={updateTodo(${todo?.id})}
                class="w-[10%] h-8 flex justify-center items-center text-white cursor-pointer bg-green-600">
                <i class="fa-solid fa-check"></i>
            </div>
            <div onclick={deleteTodo(${todo?.id})}
                class="w-[10%] h-8 flex justify-center items-center text-white cursor-pointer bg-pink-700">
                <i class="fa-solid fa-trash-can"></i>
            </div>
        </div>
    </li>`;
      }
    });
    completedItems.innerHTML = completedTodos.join("");
  }
};
//show filtered todos
// show todos
const showTodos = (e) => {
  const allItems = document.querySelector("#all");
  const pendingItems = document.querySelector("#pending");
  const completedItems = document.querySelector("#completed");
  const todos = localStorage.getItem("todos");
  const allTodos = JSON.parse(todos);
  const todosItem = allTodos.map((todo) => {
    if (todo.isComplete == true) {
      return ` <li id="${todo?.id}" class="mb-2 last:mb-0">
        <div class="w-full flex items-center space-x-1">
            <div class="h-8 bg-gray-50 w-[80%] flex items-center px-4">
                <p class="line-through w-full">${todo?.todoInfo}</p>
            </div>
            <div onclick={updateTodo(${todo?.id})}
                class="w-[10%] h-8 flex justify-center items-center text-white cursor-pointer bg-green-600">
                <i class="fa-solid fa-check"></i>
            </div>
            <div onclick={deleteTodo(${todo?.id})}
                class="w-[10%] h-8 flex justify-center items-center text-white cursor-pointer bg-pink-700">
                <i class="fa-solid fa-trash-can"></i>
            </div>
        </div>
    </li>`;
    } else {
      return ` <li id="${todo?.id}" class="mb-2 last:mb-0">
          <div class="w-full flex items-center space-x-1 ">
              <div class="h-8 bg-gray-50 w-[80%] flex items-center px-4">
                  <p >${todo?.todoInfo}</p>
              </div>
              <div onclick={updateTodo(${todo?.id})}
                  class="w-[10%] h-8 flex justify-center items-center text-white cursor-pointer bg-green-600">
                  <i class="fa-solid fa-check"></i>
              </div>
              <div onclick={deleteTodo(${todo?.id})}
                  class="w-[10%] h-8 flex justify-center items-center text-white cursor-pointer bg-pink-700">
                  <i class="fa-solid fa-trash-can"></i>
              </div>
          </div>
      </li>`;
    }
  });
  allItems.innerHTML = todosItem.join("");
  showFilteredTodos(allTodos, true);
  showFilteredTodos(allTodos, false);
};
showTodos();
// show todos

// update todo
const updateTodo = (id) => {
  const todos = localStorage.getItem("todos");
  let allTodos = JSON.parse(todos);

  const singleTodo = allTodos.find((todo) => todo.id == id);
  console.log("singleTodo", singleTodo);

  let updatedTodo = allTodos.map((upTodo) => {
    return upTodo?.id == id ? { ...upTodo, isComplete: true } : upTodo;
  });

  localStorage.setItem("todos", JSON.stringify(updatedTodo));
  showTodos();
};
// update todo
// delete todo
const deleteTodo = (id) => {
  const todos = localStorage.getItem("todos");
  let allTodos = JSON.parse(todos);

  const afterDelTodos = allTodos.filter((todo) => todo.id != id);

  localStorage.setItem("todos", JSON.stringify(afterDelTodos));
  showTodos();
};
// delete todo
