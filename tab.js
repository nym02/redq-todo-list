const tabNav = document.querySelectorAll(".tab-item");
const tabContent = document.querySelectorAll(".tab-content");

tabNav.forEach((tab, i) => {
  tab.addEventListener("click", (e) => {
    tabNav.forEach((tab, i) => {
      tab.style.borderBottom = "none";
      tab.style.color = "";
      tabContent[i].style.display = "none";
    });
    tab.style.borderBottom = "1px solid black";
    tab.style.color = "black";
    tabContent[i].style.display = "block";
  });
});

tabNav[0].click();
